import 'dart:io';

import 'Posfix.dart';

void main(List<String> args) {
  String? str = stdin.readLineSync();
  String string = str!;
  List<String> list = string.split(" ");
  List<String> postfix = infixtopostfix(list);
  evaluate(postfix);
}

void evaluate(List<String> postfix) {
  List<double> values = [];
  postfix.forEach((element) {
    if (int.tryParse(element) != null) {
      values.add(double.parse(element));
    } else {
      double right = values.removeLast();
      double left = values.removeLast();
      switch (element) {
        case "+":
          values.add(left + right);
          break;
        case "-":
          values.add(left - right);
          break;
        case "*":
          values.add(left * right);
          break;
        case "/":
          values.add(left / right);
          break;
        default:
          break;
      }
    }
  });

  print(values.first);
}
