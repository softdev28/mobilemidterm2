import 'dart:io';

void main(List<String> args) {
  String? str = stdin.readLineSync();
  String string = str!;
  List<String> list = string.split(" ");
  List<String> postfix = infixtopostfix(list);

  for (var element in postfix) {
    print(element);
  }
}

List<String> infixtopostfix(List<String> list) {
  List<String> operators = [];
  List<String> postfix = [];

  list.forEach((element) {
    if (int.tryParse(element) != null) {
      postfix.add(element);
    }
    if (isOperator(element)) {
      while (!operators.isEmpty &&
          operators.last != "(" &&
          precedence(element) <= precedence(operators.last)) {
        postfix.add(operators.removeLast());
      }
      operators.add(element);
    }
    if (element == "(") {
      operators.add(element);
    }
    if (element == ")") {
      while (operators.last != "(") {
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  });
  while (!operators.isEmpty) {
    postfix.add(operators.removeLast());
  }
  return postfix;
}

bool isOperator(String element) {
  switch (element) {
    case "+":
      return true;
    case "-":
      return true;
    case "*":
      return true;
    case "/":
      return true;
  }
  return false;
}

int precedence(String element) {
  switch (element) {
    case "+":
      return 1;
    case "-":
      return 1;
    case "*":
      return 2;
    case "/":
      return 2;
    case "(":
      return 3;
  }
  return -1;
}
